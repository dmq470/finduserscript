// ==UserScript==
// @name       FindTest
// @namespace  http://hibbard.eu/
// @version    0.1
// @description  Opens all of the links from the CodeProject newsletter in one go
// @match      http://online.wsj.com/*
// @copyright  2012+, hibbard.eu
// @require http://code.jquery.com/jquery-latest.js
// ==/UserScript==

$(document).ready( function() {
    //var elements = $('body');
    //console.log("num body elements: " + elements.count());
    // Get all body links
    // Only use the first one

	$('body').append('<input type="text" id="CP" value="" size="20" autofocus/> <button  id="CP2">Find Next</button>');

	$("#CP").css("position", "fixed").css("top", 0).css("left", 0);
    $("#CP2").css("position","fixed").css("top", 0).css("left", 100);
	$('#CP2').click( function() {
        console.log("checking");
        var str = document.getElementById ("CP").value;
        var found = window.find(str);
        //if (!found) { alert("The following text was not found:\n" + "better"); }
        //else { alert("Your browser does not support this example!"); }
	});

    
});